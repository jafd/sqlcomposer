# -*- coding: utf-8 -*-

# Initializing works

import unittest

from sqlcomposer.error import InvalidClause, MissingClause
from sqlcomposer.postgresql import SQL


class CSQLBasicTest(unittest.TestCase):

    def test_initialization(self):
        s = SQL()
        self.assertIsInstance(s, SQL)

    def test_str_on_empty_produces_errors(self):
        s = SQL()
        with self.assertRaises(MissingClause):
            s.SELECT
        with self.assertRaises(MissingClause):
            s.UPDATE
        with self.assertRaises(MissingClause):
            s.DELETE
        with self.assertRaises(MissingClause):
            s.INSERT

    def test_sql_identifiers(self):
        s = SQL()
        self.assertEqual(
            s.sql_identifier('LEFT.ANALYZE'), '"LEFT"."ANALYZE"'
        )
        self.assertEqual(
            s.sql_identifier('table."column"'), '"table"."column"'
        )
        self.assertEqual(
            s.sql_identifier('"table"."column"'), '"table"."column"'
        )
        self.assertEqual(
            s.sql_identifier('"table".foo'), '"table".foo'
        )
        self.assertEqual(
            s.sql_identifier('table.foo'), '"table".foo'
        )
        self.assertEqual(
            s.sql_identifier('table   . foo'), '"table"   . foo'
        )
        self.assertEqual(
            s.sql_identifier('table   . foo bar'), '"table"   ." foo bar"'
        )
        self.assertEqual(
            s.sql_identifier('table2   . foo bar'), 'table2   ." foo bar"'
        )


class DeleteTest(unittest.TestCase):

    def test_delete_basic(self):
        s = SQL()
        s.set_target('table1')
        self.assertEqual(s.DELETE, u'DELETE FROM table1')

    def test_delete_aliased_target(self):
        s = SQL()
        s.set_target('table1', 'foo')
        self.assertEqual(s.DELETE, u'DELETE FROM table1 AS foo')
        s.set_target('"table 1"')
        self.assertEqual(s.DELETE, u'DELETE FROM "table 1"')
        s.set_target('"table 1"', '9Long Names')
        self.assertEqual(s.DELETE, u'DELETE FROM "table 1" AS "9Long Names"')

    def test_delete_where(self):
        s = SQL()
        s.set_target('table1')
        s.add_where('column1 IS NULL')
        self.assertEqual(s.DELETE, u'DELETE FROM table1 WHERE column1 IS NULL')
        s.add_source('table2')
        s.add_where('table1.id_column = table2.id_column')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 WHERE column1 IS NULL '
            u'AND table1.id_column = table2.id_column'
        )
        s.add_source(
            'table3', alias='bar', join_type='LEFT JOIN',
            condition='bar.x = table2.x'
        )
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar'
            u' ON (bar.x = table2.x) WHERE column1 IS NULL AND'
            u' table1.id_column = table2.id_column'
        )

    def test_delete_returning(self):
        s = SQL()
        s.set_target('table1')
        s.add_returning('column1')
        s.add_returning('id_column')
        self.assertEqual(
            s.DELETE, u'DELETE FROM table1 RETURNING column1, id_column'
        )
        s.add_returning('id_column + 54', 'silly alias')
        s.add_source_column('dummy_source')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 RETURNING column1, id_column, id_column + 54 '
            u'AS "silly alias"'
        )

    def test_delete_using_where_returning(self):
        s = SQL()
        s.set_target('table1')
        s.add_where('column1 IS NULL')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 WHERE column1 IS NULL'
        )
        s.add_source('table2')
        s.add_where('table1.id_column = table2.id_column')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 WHERE column1 IS NULL AND '
            u'table1.id_column = table2.id_column'
        )
        s.add_source(
            'table3', alias='bar', join_type='LEFT JOIN',
            condition='bar.x = table2.x'
        )
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar '
            u'ON (bar.x = table2.x) WHERE column1 IS NULL '
            u'AND table1.id_column = table2.id_column'
        )
        s.add_returning('column1')
        s.add_returning('id_column')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar'
            u' ON (bar.x = table2.x) WHERE column1 IS NULL AND'
            u' table1.id_column = table2.id_column'
            u' RETURNING column1, id_column'
        )
        s.add_returning('id_column + 54', 'silly alias')
        s.add_source_column('dummy_source')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar '
            u'ON (bar.x = table2.x) '
            u'WHERE column1 IS NULL AND table1.id_column = table2.id_column '
            u'RETURNING column1, id_column, id_column + 54 AS "silly alias"'
        )

    def test_delete_comprehensive(self):
        #
        # We need these comprehensive tests to make sure all clauses
        # go in correct order
        #
        s = SQL()
        s.set_target('table1')
        s.add_where('column1 IS NULL')
        self.assertEqual(s.DELETE, u'DELETE FROM table1 WHERE column1 IS NULL')
        s.add_source('table2')
        s.add_where('table1.id_column = table2.id_column')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 WHERE column1 IS NULL '
            u'AND table1.id_column = table2.id_column'
        )
        s.add_source(
            'table3', alias='bar', join_type='LEFT JOIN',
            condition='bar.x = table2.x'
        )
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar '
            u'ON (bar.x = table2.x) '
            u'WHERE column1 IS NULL AND table1.id_column = table2.id_column'
        )
        s.add_returning('column1')
        s.add_returning('id_column')
        self.assertEqual(
            s.DELETE,
            u'DELETE FROM table1 USING table2 LEFT JOIN table3 AS bar '
            u'ON (bar.x = table2.x) WHERE column1 IS NULL AND '
            u'table1.id_column = table2.id_column RETURNING column1, id_column'
        )
        s.add_returning('id_column + 54', 'silly alias')
        self.assertEqual(
            s.DELETE, u'DELETE FROM table1 USING table2 LEFT JOIN table3 '
            u'AS bar ON (bar.x = table2.x) WHERE column1 IS NULL AND '
            u'table1.id_column = table2.id_column RETURNING column1, '
            u'id_column, id_column + 54 AS "silly alias"'
        )
        s.add_cte('(SELECT z FROM baz)', alias='bazzes')
        s.add_cte('(SELECT z FROM fuz)', alias='fuzzes', recursive=True)
        self.assertEqual(
            s.DELETE, u'WITH bazzes AS (SELECT z FROM baz), RECURSIVE '
            u'fuzzes AS (SELECT z FROM fuz) DELETE FROM table1 USING table2 '
            u'LEFT JOIN table3 AS bar ON (bar.x = table2.x) WHERE column1 '
            u'IS NULL AND table1.id_column = table2.id_column '
            u'RETURNING column1, id_column, id_column + 54 AS "silly alias"'
        )
        s.add_having('SOME_BOGUS_CONDITION()')
        self.assertEqual(
            s.DELETE,
            u'WITH bazzes AS (SELECT z FROM baz), RECURSIVE fuzzes AS '
            u'(SELECT z FROM fuz) DELETE FROM table1 USING table2 LEFT JOIN '
            u'table3 AS bar ON (bar.x = table2.x) WHERE column1 IS NULL '
            u'AND table1.id_column = table2.id_column RETURNING column1, '
            u'id_column, id_column + 54 AS "silly alias"',
            'The HAVING clause should be ignored for DELETE statements'
        )
        s.add_group_by('grouping_column')
        self.assertEqual(
            s.DELETE,
            u'WITH bazzes AS (SELECT z FROM baz), RECURSIVE fuzzes AS '
            u'(SELECT z FROM fuz) DELETE FROM table1 USING table2 LEFT JOIN '
            u'table3 AS bar ON (bar.x = table2.x) WHERE column1 IS NULL AND '
            u'table1.id_column = table2.id_column RETURNING column1, '
            u'id_column, id_column + 54 AS "silly alias"',
            'The GROUP BY clause should be ignored for DELETE statements'
        )
        s.add_order_by('ordering_column')
        self.assertEqual(
            s.DELETE,
            u'WITH bazzes AS (SELECT z FROM baz), RECURSIVE fuzzes AS '
            u'(SELECT z FROM fuz) DELETE FROM table1 USING table2 '
            u'LEFT JOIN table3 AS bar ON (bar.x = table2.x) WHERE '
            u'column1 IS NULL AND table1.id_column = table2.id_column '
            u'RETURNING column1, id_column, id_column + 54 AS "silly alias"',
            'The ORDER BY clause should be ignored for DELETE statements'
        )


class SelectTest(unittest.TestCase):

    def test_select_basic(self):
        s = SQL()
        s.add_source_column('NOW()')
        self.assertEqual(s.SELECT, u'SELECT NOW()')

    def test_select_columns(self):
        s = SQL()
        s.add_source_column('NOW()', 'curtime')
        self.assertEqual(s.SELECT, u'SELECT NOW() AS curtime')
        s.add_source_column('"table"."column"', 'long alias')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table"."column" AS "long alias"'
        )
        s.add_source_column('"table".other_column + 3', 'ćółümn')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table"."column" AS "long alias", '
            u'"table".other_column + 3 AS "ćółümn"'
        )
        s.add_source_column('"table".other_column + 4', u'ćółümn2')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table"."column" AS "long alias", '
            u'"table".other_column + 3 AS "ćółümn", '
            u'"table".other_column + 4 AS "ćółümn2"'
        )

    def test_select_comprehensive(self):
        self.maxDiff = None
        s = SQL()
        s.add_source_column('NOW()', 'curtime')
        self.assertEqual(s.SELECT, u'SELECT NOW() AS curtime')
        s.add_source_column('"table".column', 'long alias')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias"'
        )
        s.add_source('table1', alias='table')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table"'
        )
        s.add_source(
            'table2',
            alias='table_2',
            join_type='JOIN',
            condition='table_2.id = "table".id'
        )
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id)'
        )
        s.add_source('table3', alias='t2')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2'
        )
        s.add_source(
            'table4', alias='t4', join_type='LEFT JOIN', using='id'
        )
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" '
            u'JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id)'
        )
        s.add_source('table5', alias='t5', using='id')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 '
            u'ON (table_2.id = "table".id), table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id)'
        )
        s.add_where('t4.foo = 21')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" FROM '
            u'table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2 LEFT JOIN table4 AS t4 '
            u'USING (id) JOIN table5 AS t5 USING (id) '
            u'WHERE t4.foo = 21'
        )
        s.add_group_by(u'c1')
        s.add_group_by(u'c2')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2 LEFT JOIN table4 AS t4 '
            u'USING (id) JOIN table5 AS t5 USING (id) '
            u'WHERE t4.foo = 21 GROUP BY c1, c2'
        )
        s.add_where('t4.bar = \'test value\'')
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2 LEFT JOIN table4 AS t4 '
            u'USING (id) JOIN table5 AS t5 USING (id) WHERE t4.foo = 21 '
            u'AND t4.bar = \'test value\' GROUP BY c1, c2'
        )
        s.add_having('t4.havingfoo = 21')
        s.add_having('t4.bar = \'having test value\'')

        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 ON '
            u'(table_2.id = "table".id), table3 AS t2 LEFT JOIN table4 AS t4 '
            u'USING (id) JOIN table5 AS t5 USING (id) WHERE t4.foo = 21 '
            u'AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\''
        )

        s.set_limit(100)
        self.assertEqual(
            s.SELECT,
            u'SELECT NOW() AS curtime, "table".column AS "long alias" '
            u'FROM table1 AS "table" JOIN table2 AS table_2 '
            u'ON (table_2.id = "table".id), table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100'
        )

        s.set_distinct(True)
        self.assertEqual(
            s.SELECT,
            u'SELECT DISTINCT NOW() AS curtime, '
            u'"table".column AS "long alias" '
            u'FROM table1 AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100'
        )

        s.set_distinct(True, on='("table".column)')
        self.assertEqual(
            s.SELECT,
            u'SELECT DISTINCT ON ("table".column) NOW() AS curtime, '
            u'"table".column AS "long alias" FROM table1 AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100'
        )
        s2 = SQL()
        s2.add_source_column('otherworld.country')
        s2.add_source('discworld', alias='otherworld')
        s2.add_where('otherworld.population > 1000000')

        s.add_cte(s2, alias='bigcountries')
        s.add_source_column('bigcountries.country')
        s.add_source(
            'bigcountries',
            join_type='LEFT JOIN',
            condition='bigcountries.country=table2.country'
        )

        self.assertEqual(
            s.SELECT,
            u'WITH bigcountries AS (SELECT otherworld.country FROM discworld '
            u'AS otherworld '
            u'WHERE otherworld.population > 1000000) '
            u'SELECT DISTINCT ON ("table".column) NOW() '
            u'AS curtime, "table".column AS "long alias", '
            u'bigcountries.country '
            u'FROM table1 AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'LEFT JOIN bigcountries ON (bigcountries.country=table2.country) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100'
        )

        s.set_select_locking('FOR UPDATE')
        self.assertEqual(
            s.SELECT,
            u'WITH bigcountries AS ('
            u'SELECT otherworld.country FROM discworld AS otherworld '
            u'WHERE otherworld.population > 1000000) '
            u'SELECT DISTINCT ON ("table".column) NOW() '
            u'AS curtime, "table".column AS "long alias", '
            u'bigcountries.country FROM table1 '
            u'AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'LEFT JOIN bigcountries ON (bigcountries.country=table2.country) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100 FOR UPDATE'
        )

        s.set_select_locking('FOR SHARE OF TABLE table1 SKIP LOCKED')
        self.assertEqual(
            s.SELECT,
            u'WITH bigcountries AS ('
            u'SELECT otherworld.country FROM discworld AS otherworld '
            u'WHERE otherworld.population > 1000000) '
            u'SELECT DISTINCT ON ("table".column) NOW() '
            u'AS curtime, "table".column AS "long alias", '
            u'bigcountries.country FROM table1 '
            u'AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'LEFT JOIN bigcountries ON (bigcountries.country=table2.country) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'LIMIT 100 '
            u'FOR SHARE OF TABLE table1 SKIP LOCKED'
        )

        s.add_order_by('bigcountries.country', direction='DESC', nulls='LAST')
        self.assertEqual(
            s.SELECT,
            u'WITH bigcountries AS (SELECT otherworld.country '
            u'FROM discworld AS otherworld '
            u'WHERE otherworld.population > 1000000) '
            u'SELECT DISTINCT ON ("table".column) NOW() '
            u'AS curtime, "table".column AS "long alias", '
            u'bigcountries.country FROM table1 '
            u'AS "table" '
            u'JOIN table2 AS table_2 ON (table_2.id = "table".id), '
            u'table3 AS t2 '
            u'LEFT JOIN table4 AS t4 USING (id) JOIN table5 AS t5 USING (id) '
            u'LEFT JOIN bigcountries ON (bigcountries.country=table2.country) '
            u'WHERE t4.foo = 21 AND t4.bar = \'test value\' GROUP BY c1, c2 '
            u'HAVING t4.havingfoo = 21 AND t4.bar = \'having test value\' '
            u'ORDER BY bigcountries.country DESC NULLS LAST '
            u'LIMIT 100 '
            u'FOR SHARE OF TABLE table1 SKIP LOCKED'
        )

    def test_orderby_basic(self):
        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor_log')

        s.add_order_by('logmoment::date', direction='desc')
        s.add_order_by('value', direction='asc')

        self.assertEqual(s.SELECT, u'SELECT value FROM sensor_log '
                                   u'ORDER BY logmoment::date DESC, value ASC')

    def test_orderby_advanced(self):
        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor_log')

        with self.assertRaises(InvalidClause):
            s.add_order_by(
                'logmoment::date',
                direction='desc',
                using='>'
            )
        with self.assertRaises(InvalidClause):
            s.add_order_by(
                'logmoment::date',
                direction='desc',
                nulls='IN THE MIDDLE'
            )
        with self.assertRaises(InvalidClause):
            s.add_order_by('logmoment::date', direction='sideways')

        s.add_order_by('logmoment', using='<')

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor_log '
            u'ORDER BY logmoment USING <'
        )

        s.add_order_by('value', nulls='first')

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor_log '
            u'ORDER BY logmoment USING <, value NULLS FIRST'
        )

    def test_union(self):
        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_union(s_union)

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'UNION SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_union(s_union, mode='ALL')

        self.assertEqual(
            s.SELECT, u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'UNION ALL SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

    def test_intersect(self):
        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_intersect(s_union)

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'INTERSECT SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_intersect(s_union, mode='ALL')

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'INTERSECT ALL SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

    def test_except(self):
        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_except(s_union)

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'EXCEPT SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        s.add_except(s_union, mode='ALL')

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'EXCEPT ALL SELECT value FROM sensor2_log '
            u'ORDER BY logmoment DESC'
        )

    def test_union_intersect_except(self):
        self.maxDiff = None

        s = SQL()
        s.add_source_column('value')
        s.add_source('sensor1_log')
        s.add_where('value > 2')
        s.add_order_by('logmoment', direction='desc')

        s_union = SQL()
        s_union.add_source_column('value')
        s_union.add_source('sensor2_log')
        s_union.add_order_by('sensor2_log.logmoment', direction='desc')
        s_union.set_select_locking('FOR SHARE')

        with self.assertRaises(InvalidClause):
            s.add_union(s_union, mode='UMMPH')

        s.add_union(s_union)

        s_intersect = SQL()
        s_intersect.add_source_column('value')
        s_intersect.add_source('sensor3_log')
        s_intersect.add_order_by('sensor3_log.logmoment', direction='desc')
        s_intersect.set_select_locking('FOR SHARE')

        with self.assertRaises(InvalidClause):
            s.add_intersect(s_intersect, mode='UMMPH')
        s.add_intersect(s_intersect)

        s_except = SQL()
        s_except.add_source_column('value')
        s_except.add_source('sensor4_log')
        s_except.add_order_by('sensor4_log.logmoment', direction='desc')
        s_except.set_select_locking('FOR SHARE')

        with self.assertRaises(InvalidClause):
            s.add_except(s_except, mode='UMMPH')
        s.add_except(s_except)

        self.assertEqual(
            s.SELECT,
            u'SELECT value FROM sensor1_log WHERE value > 2 '
            u'UNION SELECT value FROM sensor2_log '
            u'INTERSECT SELECT value FROM sensor3_log '
            u'EXCEPT SELECT value FROM sensor4_log '
            u'ORDER BY logmoment DESC'
        )

    def test_from_clauses(self):
        s = SQL()
        s.add_source_column('*')
        s.add_source('sensor_log')
        self.assertEqual(s.SELECT, u'SELECT * FROM sensor_log')
        s = SQL()
        s.add_source_column('*')
        s.add_source('sensor_log', only=True)
        self.assertEqual(s.SELECT, u'SELECT * FROM ONLY sensor_log')
        s = SQL()
        s.add_source_column('*')
        s.add_source('sensor_log', only=True)
        s.add_source('weekdays', join_type='LATERAL JOIN')
        self.assertEqual(
            s.SELECT,
            u'SELECT * FROM ONLY sensor_log LATERAL JOIN weekdays'
        )

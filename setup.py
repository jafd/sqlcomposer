#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='sqlcomposer',
    version='0.0.1',
    description='A library to compose SQL statements in a more Pythonic way',
    long_description=open('README', 'r').read(),
    url='https://bitbucket.com/jafd/sqlcomposer/',
    license='MIT',

    packages=find_packages(exclude='tests'),
    include_package_data=False,
    zip_safe=True,

    install_requires=(
    ),

    tests_require=(
        'nose',
    ),

    test_suite='nose.collector',

    extras_require={
        'dev': (
            'sphinx'
        ),
    },

    entry_points={
    },

    cmdclass={
    },

    classifiers=(
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ),

    author='Yaroslav Fedevych',
    author_email='yaroslav@fedevych.name',
)

# -*- coding: utf-8 -*-


def nonemptystr(inlist, sep=u' '):
    """
    Utility function. Returns a Unicode object composed of non-false
    list elements joined by separator.

    :param inlist: the list of elements (either Unicode objects
                   or convertable to Unicode)
    :param sep: separator
    :return: unicode
    """
    if isinstance(inlist, basestring):
        return inlist
    return sep.join([unicode(x) for x in inlist if x])



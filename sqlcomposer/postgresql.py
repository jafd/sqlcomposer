# -*- coding: utf-8 -*-

from .util import nonemptystr
from .base import BaseSQL, \
                  SupportsCTEs, \
                  SupportsReturning, \
                  SupportsDistinctOn
from .error import MissingClause


POSTGRESQL_RESERVED = (
    'all', 'analyse', 'analyze', 'and', 'any', 'array', 'as', 'asc',
    'asymmetric', 'authorization', 'binary', 'both', 'case', 'cast',
    'check', 'collate', 'collation', 'column', 'concurrently', 'constraint',
    'create', 'cross', 'current_catalog', 'current_date', 'current_role',
    'current_schema', 'current_time', 'current_timestamp', 'current_user',
    'default', 'deferrable', 'desc', 'distinct', 'do', 'else', 'end',
    'except', 'false', 'fetch', 'for', 'foreign', 'freeze', 'from', 'full',
    'grant', 'group', 'having', 'ilike', 'in', 'initially', 'inner',
    'intersect', 'into', 'is', 'isnull', 'join', 'lateral', 'leading',
    'left', 'like', 'limit', 'localtime', 'localtimestamp', 'natural',
    'not', 'notnull', 'null', 'offset', 'on', 'only', 'or', 'order',
    'outer', 'overlaps', 'placing', 'primary', 'references', 'returning',
    'right', 'select', 'session_user', 'similar', 'some', 'symmetric',
    'table', 'tablesample', 'then', 'to', 'trailing', 'true', 'union',
    'unique', 'user', 'using', 'variadic', 'verbose', 'when', 'where',
    'window', 'with'
)


class SQL(
    BaseSQL,
    SupportsCTEs,
    SupportsReturning,
    SupportsDistinctOn
):
    def __init__(
        self,
        encoding=None,
        default_representation='SELECT',
    ):
        """
        Initialize.
        """
        super(SQL, self).__init__(
            encoding=encoding,
            default_representation=default_representation,
            reserved_words=POSTGRESQL_RESERVED,
            quote_character='"',
            escaped_quote_character='\\"'
        )

    @property
    def INSERT(self):
        """
        Returns representation of itself as an INSERT
        statement.

        :return: unicode
        """
        if not self.target:
            raise MissingClause('Target table for INSERT not set')
        pass

    @property
    def UPDATE(self):
        """
        Returns representation of itself as an UPDATE
        statement.

        :return: unicode
        """
        if not self.target:
            raise MissingClause('Target table for UPDATE not set')
        pass

    @property
    def DELETE(self):
        """
        Returns representation of itself as a DELETE
        statement.

        :return: unicode
        """
        if not self.target:
            raise MissingClause('Target table for DELETE not set')
        only = u''
        if self.target.only:
            only = u'ONLY '
        withlist = self.format_cte_list()
        usinglist = self.format_list(self.sources, prefix=u'USING')
        returninglist = self.format_returning_list()
        where = self.format_where_clauses(self.where)
        deleteclause = u'DELETE FROM %s%s' % (only, self.target)
        return nonemptystr([
            withlist, deleteclause, usinglist, where, returninglist
        ])

    @property
    def SELECT(self):
        """
        Returns representation of itself as a SELECT
        statement.

        :return: unicode
        """
        if len(self.source_columns) == 0:
            raise MissingClause('Cannot produce SELECT - no columns to select')
        withlist = self.format_cte_list()
        collist = self.format_list(self.source_columns, prefix=u'')
        fromlist = self.format_list(self.sources)
        wherelist = self.format_where_clauses(self.where)
        grouplist = self.format_list(self.group_by, prefix=u'GROUP BY')
        havinglist = self.format_where_clauses(self.having, prefix=u'HAVING')
        orderlist = self.format_order_list(self.order_by)
        unionlist = self.format_union_list(self.unions)
        limit = u''
        if self.limit is not None:
            limit = u'LIMIT %s' % self.limit

        distinct = []
        if self.distinct:
            distinct.append(u'DISTINCT')
            if self.distinct_on:
                distinct.append(u'ON')
                distinct.append(self.distinct_on)
        return nonemptystr([
            withlist, u'SELECT', nonemptystr(distinct), collist, fromlist,
            wherelist, grouplist, havinglist, unionlist, orderlist,
            limit, self.locking or u''
        ])

    @property
    def SELECT_FOR_UNION(self):
        """
        Returns representation of itself as a SELECT
        statement, but omitting ORDER BY, LIMIT, OFFSET, locking

        :return: unicode
        """
        if len(self.source_columns) == 0:
            raise MissingClause('Cannot produce SELECT - no columns to select')
        withlist = self.format_cte_list()
        collist = self.format_list(self.source_columns, prefix=u'')
        fromlist = self.format_list(self.sources)
        wherelist = self.format_where_clauses(self.where)
        grouplist = self.format_list(self.group_by, prefix=u'GROUP BY')
        havinglist = self.format_where_clauses(self.having, prefix=u'HAVING')

        distinct = self.format_distinct_clause()
        return nonemptystr([
            withlist, u'SELECT', nonemptystr(distinct), collist, fromlist,
            wherelist, grouplist, havinglist
        ])


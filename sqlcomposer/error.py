"""
Exceptions that SQL class can throw.
"""


class SQLError(Exception):
    pass


class MissingClause(SQLError):
    pass


class InvalidClause(SQLError):
    pass


class DuplicateAlias(SQLError):
    pass

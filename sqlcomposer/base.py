# -*- coding: utf-8 -*-

from .__init__ import default_encoding
from .error import InvalidClause
from .util import nonemptystr
import re


class _SQL(object):

    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        self.encoding = encoding
        self.reserved_words = reserved_words
        self.default_representation = default_representation
        self.quote_character = quote_character
        self.escaped_quote_character = escaped_quote_character

    def unicodify(self, instr):
        if isinstance(instr, str):
            return instr.decode(self.encoding or default_encoding or 'utf-8')
        return instr

    def sql_identifier(self, insql):
        insql = self.unicodify(insql)
        resbuffer = []

        pieces = insql.split('.')

        for piece in pieces:
            spiece = piece.strip()
            if (
                spiece.lower() in self.reserved_words
            ):
                m = re.match(
                    r'^(\s*)([^\s]+?)(\s*)$',
                    piece
                )
                resbuffer.append(u'%s%s%s%s%s' % (
                    m.group(1),
                    self.quote_character,
                    m.group(2).replace(
                        self.quote_character,
                        self.escaped_quote_character
                    ),
                    self.quote_character,
                    m.group(3)
                ))
            elif not (
                re.match(r'^[A-Za-z_][A-Za-z0-9_]*$', spiece) or
                (
                    spiece.startswith(self.quote_character) and
                    spiece.endswith(self.quote_character)
                )
            ):
                resbuffer.append(u'%s%s%s' % (
                    self.quote_character,
                    piece.replace(
                        self.quote_character,
                        self.escaped_quote_character
                    ),
                    self.quote_character,
                ))
            else:
                resbuffer.append(piece)
        return u'.'.join(resbuffer)

    @staticmethod
    def format_list(tables, prefix=u'FROM'):
        fromlist = [
            [' ' if x.join_type else ', ', unicode(x)]
            for x in tables
        ]
        if len(fromlist) == 0:
            return u''
        fromlist[0][0] = u''
        return nonemptystr([prefix, u''.join([u''.join(x) for x in fromlist])])


class _Expression(_SQL):
    """
    A "kitchen sink" of SQL expressions appearing as list items in clauses.
    You don't need to use this object directly. It's used by the SQL object
    itself.
    """
    def __init__(
        self, expression, lefthand=None, alias=None, recursive=None,
        only=False, join_type=None, condition=None, encoding=None,
        using=None, reserved_words=None, quote_character='"',
        escaped_quote_character='\\"'
    ):
        super(_Expression, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.lefthand = lefthand
        self.expression = expression
        self.recursive = recursive
        self.alias = alias
        self.only = only
        self.join_type = join_type
        self.condition = condition
        self.using = using

    def __unicode__(self):

        only = u''
        if self.only:
            only = u'ONLY'

        recursive = u''
        if self.recursive:
            recursive = u'RECURSIVE'

        if (isinstance(self.expression, BaseSQL)):
            expuni = u'(%s)' % (unicode(self.expression))
        else:
            expuni = unicode(self.expression)

        lefthand = u''
        if self.lefthand:
            lefthand = u'%s =' % (self.sql_identifier(self.lefthand))

        buffer = [self.join_type, only, recursive, lefthand, expuni]

        if self.alias:
            buffer.append(u'AS %s' % self.sql_identifier(self.alias))

        if self.condition:
            buffer.append(u'ON (%s)' % self.unicodify(self.condition))

        if self.using:
            buffer.append(u'USING (%s)' % self.unicodify(self.using))

        return nonemptystr(buffer)


class _CTEExpression(_Expression):
    """
    CTEs have "alias AS expression" convention, and the alias is required
    """
    def __unicode__(self):
        recursive = u''
        if self.recursive:
            recursive = u'RECURSIVE'

        if not self.alias:
            raise InvalidClause('Need alias for WITH clause')
        alias = u'%s AS' % self.sql_identifier(self.alias)

        if isinstance(self.expression, BaseSQL):
            expuni = u'(%s)' % (unicode(self.expression))
        else:
            expuni = unicode(self.expression)

        lefthand = u''
        if self.lefthand:
            lefthand = u'%s =' % (self.sql_identifier(self.lefthand))

        buffer = [recursive, alias, lefthand, expuni]

        return nonemptystr(buffer)


class SupportsCTEs(_SQL):
    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        """
        Initialize.
        """
        super(SupportsCTEs, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            default_representation=default_representation,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.cte = []

    def add_cte(self, cte, alias=None, recursive=False):
        self.cte.append(_CTEExpression(
            cte,
            alias=alias,
            recursive=recursive,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    def format_cte_list(self):
        return self.format_list(self.cte, prefix=u'WITH')


class SupportsReturning(_SQL):
    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        """
        Initialize.
        """
        super(SupportsReturning, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            default_representation=default_representation,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.returning = []

    def add_returning(self, returning_spec, alias=None):
        self.returning.append(_Expression(
            returning_spec,
            alias=alias,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    def format_returning_list(self):
        return self.format_list(self.returning, prefix=u'RETURNING')


class SupportsBasicDistinct(_SQL):

    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        """
        Initialize.
        """
        super(SupportsBasicDistinct, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            default_representation=default_representation,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.distinct = None

    def set_distinct(self, distinct):
        if distinct is None:
            self.distinct = None
        self.distinct = bool(distinct)

    def format_distinct_clause(self):
        if self.distinct is None:
            return u''
        elif self.distinct:
            return u'DISTINCT'
        else:
            return u'ALL'


class SupportsDistinctOn(_SQL):

    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        """
        Initialize.
        """
        super(SupportsDistinctOn, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            default_representation=default_representation,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.distinct = None
        self.distinct_on = []

    def set_distinct(self, distinct, on=None):
        if distinct:
            self.distinct = distinct
            if on:
                self.distinct_on = on
        else:
            self.distinct = None
            self.distinct_on = None

    def format_distinct_clause(self):
        distinct = []
        if self.distinct:
            distinct.append(u'DISTINCT')
            if self.distinct_on:
                distinct.append(u'ON')
                distinct.append(self.distinct_on)
        return nonemptystr(distinct)


class BaseSQL(_SQL):
    """
    An object that represent an SQL statement. It can produce any
    of the SELECT, UPDATE, DELETE, or INSERT statements from the same
    source data.

    The elements from FROM clause are called sources; target for UPDATE,
    INSERT, and DELETE is called target.

    If data for any required clause is missing, an exception is raised.
    The clause data can be added in any order.
    """
    def __init__(
        self,
        encoding=None,
        reserved_words=None,
        default_representation='SELECT',
        quote_character='"',
        escaped_quote_character='\\"'
    ):
        """
        Initialize.
        """
        super(BaseSQL, self).__init__(
            encoding=encoding,
            reserved_words=reserved_words,
            default_representation=default_representation,
            quote_character=quote_character,
            escaped_quote_character=escaped_quote_character
        )
        self.sources = []
        self.target = None
        self.source_columns = []
        self.source_columns_set = set()
        self.target_columns = []
        self.target_columns_set = set()
        self.table_aliases = set()
        self.column_aliases = set()
        self.where = []
        self.having = []
        self.group_by = []
        self.order_by = []
        self.unions = []
        self.limit = None
        self.offset = None
        self.locking = None

    def __str__(self):
        return self.__unicode__().encode(
            self.encoding or default_encoding or 'utf-8'
        )

    def __unicode__(self):
        return getattr(self, self.default_representation)

    @property
    def INSERT(self):
        raise NotImplementedError('This operation is not supported')

    @property
    def UPDATE(self):
        raise NotImplementedError('This operation is not supported')

    @property
    def SELECT(self):
        raise NotImplementedError('This operation is not supported')

    @property
    def DELETE(self):
        raise NotImplementedError('This operation is not supported')

    @property
    def SELECT_FOR_UNION(self):
        raise NotImplementedError('This operation is not supported')

    @staticmethod
    def format_union_list(unions):
        if len(unions) == 0:
            return u''
        lst = []
        for union in unions:
            statement, prefix, mode = union
            lst.append(nonemptystr([
                prefix, mode or u'', statement.SELECT_FOR_UNION
            ]))
        return u' '.join(lst)

    def add_source_column(self, column_spec, alias=None):
        """
        Adds a column to be a data source for SELECT statement,
         or a RETURNING column for INSERT, UPDATE, or DELETE
         statements.
        :param column_spec:
        :param alias:
        :return:
        """
        self.source_columns.append(_Expression(
            column_spec,
            alias=alias,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    def add_target_column(self, lefthand, value):
        """
        Adds a column to be a data target for INSERT (VALUES)
        or UPDATE statement (SET clause). Is unused with SELECT.

        :param lefthand:
        :param value:
        :return:
        """
        self.target_columns.append(_Expression(
            value,
            lefthand=lefthand,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    def _do_add_union(self, stmt, prefix, mode=None):
        if str(prefix).lower() not in ('union', 'intersect', 'except'):
            raise InvalidClause(
                'Substatement can only be attached with UNION, INTERSECT,'
                ' or EXCEPT'
            )
        if mode and str(mode).lower() not in ('all', 'distinct'):
            raise InvalidClause('UNION can only be ALL/DISTINCT')
        self.unions.append(
            (stmt, unicode(prefix).upper(), unicode(mode or '').upper())
        )

    def add_union(self, stmt, mode=None):
        return self._do_add_union(stmt, u'UNION', mode)

    def add_intersect(self, stmt, mode=None):
        return self._do_add_union(stmt, u'INTERSECT', mode)

    def add_except(self, stmt, mode=None):
        return self._do_add_union(stmt, u'EXCEPT', mode)

    def format_values_clause(self, fromlist):
        values = [unicode(x.expression) for x in fromlist]
        return u'VALUES (%s)' % (u', '.join(values))

    def format_columns_clause(self, fromlist):
        values = [self.sql_identifier(unicode(x.lefthand)) for x in fromlist]
        return u'(%s)' % (u', '.join(values))

    def add_source(
        self, table_spec, alias=None, only=False, join_type=None,
        condition=None, using=None
    ):
        self.table_aliases.add(alias)
        if condition is not None and using is not None:
            raise InvalidClause(
                'Cannot use both ON and USING in a JOIN clause'
            )
        if join_type is None and (
            using is not None or condition is not None
        ):
            join_type = u'JOIN'
        self.sources.append(_Expression(
            table_spec,
            alias=alias,
            only=only,
            join_type=join_type,
            condition=condition,
            using=using,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    def set_target(self, table_spec, alias=None, only=False):
        """
        There can only be one target table for
        INSERT, DELETE, or UPDATE statements. Thus set_,
        not add_ prefix.

        :param table_spec:
        :param only:
        :return:
        """
        self.target = _Expression(
            table_spec,
            alias=alias,
            only=only,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        )
        self.table_aliases.add(alias)

    def add_where(self, condition):
        """
        Adds a condition to be used in the WHERE clause.
        :param condition: string/unicode
        :return:
        """
        self.where.append(condition)

    def add_having(self, condition):
        self.having.append(condition)

    def add_group_by(self, column_spec):
        self.group_by.append(_Expression(
            column_spec,
            encoding=self.encoding,
            reserved_words=self.reserved_words,
            quote_character=self.quote_character,
            escaped_quote_character=self.escaped_quote_character
        ))

    @staticmethod
    def format_where_clauses(wherelist, prefix=u'WHERE'):
        if len(wherelist) == 0:
            return u''
        return u'%s %s' % (prefix, u' AND '.join(wherelist))

    def set_limit(self, limit):
        self.limit = limit

    def add_order_by(
        self, column_spec, direction=None, using=None, nulls=None
    ):
        if direction is not None and \
           str(direction).lower() not in ('asc', 'desc'):
            raise InvalidClause(
                'Invalid order specification: %s' % (direction,)
            )
        if nulls is not None and \
           str(nulls).lower() not in ('first', 'last'):
            raise InvalidClause(
                'Invalid order specification: NULLS %s' % (nulls,)
            )
        if using is not None and direction is not None:
            raise InvalidClause(
                'Invalid order specification: '
                'ASC/DESC and USING are mutually exclusive'
            )
        self.order_by.append((column_spec, direction, using, nulls))

    @staticmethod
    def format_order_list(orderby):
        if len(orderby) == 0:
            return u''
        buffer = [u'ORDER BY']
        lst = []
        for l in orderby:
            finalexpr = u''
            expression, direction, using, nulls = l
            if using and direction:
                raise InvalidClause(
                    'Use either ASC/DESC or USING in an ORDER BY, not both'
                )
            if using:
                finalexpr += u'{} USING {}'.format(expression, using)
            elif direction:
                finalexpr += u'{} {}'.format(
                    expression, unicode(direction).upper()
                )
            else:
                finalexpr += expression
            if nulls is not None and nulls.lower() in ('first', 'last'):
                finalexpr += u' NULLS {}'.format(unicode(nulls).upper())
            elif nulls is not None:
                raise InvalidClause(
                    'NULLS in an ORDER BY clause must be either FIRST or LAST'
                )
            lst.append(finalexpr)
        buffer.append(u', '.join(lst))
        return nonemptystr(buffer)

    def set_select_locking(self, locking_spec):
        """
        Specify locking for SELECT statements, like FOR UPDATE or FOR SHARE.

        :param locking_spec: str
        :return:
        """
        self.locking = locking_spec

